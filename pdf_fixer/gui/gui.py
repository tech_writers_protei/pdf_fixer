# -*- coding: utf-8 -*-
import os.path as p
from pathlib import Path
from tkinter import Canvas, Event, Misc, Text, Widget
from tkinter.constants import CENTER, DISABLED, LEFT, WORD
from tkinter.filedialog import askopenfilename
from tkinter.font import Font
from tkinter.ttk import Button, Combobox, Entry, Spinbox
from typing import Callable, Literal, TypeAlias

from PIL.ImageTk import PhotoImage

from constants import FontFamily, ImageInfo, _OUTPUT_FONT, _STEPPER_FONT
from pdf_fixer.constants import TextBox


def get_file():
    filetypes: list[tuple[str, str]] = [("PDF files", "*.pdf")]
    initialdir: str = p.join(p.expanduser("~"), "Desktop")
    title: str = "Выберите файл для обработки"
    return askopenfilename(filetypes=filetypes, initialdir=initialdir, title=title)


class StepperSpinbox(Spinbox):
    def __init__(self, master: Misc, to: float):
        from_: float = float(1)
        font: Font = _STEPPER_FONT
        justify: Literal["left", "center", "right"] = CENTER
        increment: float = float(1)
        wrap: bool = False
        name: str = "stepper_spinbox"
        super().__init__(
            master,
            name=name,
            from_=from_,
            font=font,
            increment=increment,
            justify=justify,
            to=to,
            wrap=wrap)
        self._value: str | None = "1"

    def ui(self):
        self.configure(command=lambda: self.on_click)
        self.grid()

    def on_click(self, event: Event):
        value: str = event.widget.get()
        self._value = value
        return value

    def __bool__(self):
        return self._value is not None

    def get_value(self):
        return int(self._value)


class OutputText(Text):
    def __init__(self, master: Misc, index: int):
        font: Font = _OUTPUT_FONT
        width: int = 50
        height: int = 1
        wrap: Literal["none", "word", "char"] = WORD
        state: Literal["disabled", "normal"] = DISABLED
        name: str = f"output_text_{index}"
        super().__init__(
            master,
            font=font,
            height=height,
            name=name,
            state=state,
            width=width,
            wrap=wrap)

    def set_text(self, text: str):
        self.insert(0, text)

    def get_text(self):
        return self.get("1.0", "end-1c")


class ReplaceTextEntry(Entry):
    def __init__(self, master: Misc):
        cursor: str = "xterm"
        name: str = "replace_text_entry"
        font: Font = _OUTPUT_FONT
        justify: Literal["left", "center", "right"] = LEFT
        show: str = "Введите текст для замены без кавычек"
        super().__init__(
            master,
            cursor=cursor,
            font=font,
            justify=justify,
            name=name,
            show=show)

    def set_text(self, text: str):
        self.insert(0, text)

    def get_text(self):
        return self.get()


class FontCombobox(Combobox):
    def __init__(self, master: Misc):
        name: str = "font_combobox"
        font: Font = _OUTPUT_FONT
        justify: Literal["left", "center", "right"] = "left"
        values: list[str] = list(FontFamily)
        super().__init__(
            master,
            name=name,
            font=font,
            justify=justify,
            values=values)

    def get_value(self):
        return self.get()


class Sketch(Canvas):
    def __init__(self, master: Misc, image_info: ImageInfo):
        height: int = image_info.height
        width: int = image_info.width
        background: str = "white"
        name: str = "sketch"

        super().__init__(
            master,
            height=height,
            width=width,
            name=name,
            background=background)

        self._start_point_x: float | None = None
        self._start_point_y: float | None = None
        self._end_point_x: float | None = None
        self._end_point_y: float | None = None
        self._image: int | None = None

    def ui(self):
        self.bind("<ButtonPress-1>", self.start)
        self.bind("<B1-Motion>", self.drag)
        self.bind("<ButtonPress-2>", self.clear)

    def start(self, event: Event):
        self._start_point_x = event.x
        self._start_point_y = event.y

    def drag(self, event: Event):
        self._end_point_x = min(event.x, self.winfo_width())
        self._end_point_y = min(event.y, self.winfo_height())
        self.draw()

    def draw(self):
        self.create_rectangle(
            self._start_point_x,
            self._start_point_y,
            self._end_point_x,
            self._end_point_y,
            outline="red",
            tags="rectangle",
            width=2)

    # noinspection PyUnusedLocal
    def clear(self, event: Event):
        self.delete("rectangle")

        self._start_point_x = None
        self._start_point_y = None
        self._end_point_x = None
        self._end_point_y = None

    def __key(self):
        return self._start_point_x, self._start_point_y, self._end_point_x, self._end_point_y

    def __bool__(self):
        return all(item is not None for item in self.__key())

    def _x(self):
        return self._start_point_x, self._end_point_x

    def _y(self):
        return self._start_point_y, self._end_point_y

    def text_box(self):
        if not bool(self):
            return TextBox.null()

        else:
            x_min: int = int(min(self._x()))
            x_max: int = int(max(self._x()))
            y_min: int = int(min(self._y()))
            y_max: int = int(max(self._y()))

            return TextBox(x_min, x_max, y_min, y_max)

    def __repr__(self):
        return f"<{self.__class__.__name__}> {repr(self.text_box())}"

    def __str__(self):
        return f"<{self.__class__.__name__}>"

    def set_image(self, file: Path):
        image: PhotoImage = PhotoImage(file=file)
        self._image = self.create_image(0, 0, image=image)

    def update_image(self, file: Path):
        image: PhotoImage = PhotoImage(file=file)
        self.itemconfig(self._image, image=image)
        self._image = self.create_image(0, 0, image=image)


class ActionButton(Button):
    def __init__(self, master: Misc, name: str, command: Callable = None):
        padding: list[int] = [5, 5, 5, 5]
        super().__init__(master, command=command, name=name, padding=padding)

    def add_command(self, command: Callable):
        self.configure(command=command)


WidgetType: TypeAlias = Widget | OutputText | StepperSpinbox | ReplaceTextEntry | Sketch | Combobox | ActionButton
