# -*- coding: utf-8 -*-
from enum import Enum
from tkinter.constants import NORMAL
from tkinter.font import Font, ROMAN
from typing import Literal, NamedTuple

_FORMATS: tuple[str, ...] = ("PNG", "JPG", "JPEG")

_OUTPUT_FONT: Font = Font(
    family="Times",
    size=16,
    weight=NORMAL,
    slant=ROMAN,
    underline=False,
    overstrike=False)
_STEPPER_FONT: Font = Font(
    family="Times",
    size=14,
    weight=NORMAL,
    slant=ROMAN,
    underline=False,
    overstrike=False)


class ImageInfo(NamedTuple):
    width: int
    height: int

    def __str__(self):
        return f"({self.width}, {self.height})"

    def __repr__(self):
        return f"<{self.__class__.__name__}(width={self.width}, height={self.height})>"


class FontFamily(Enum):
    ARIAL = "Arial"
    ARIAL_BLACK = "Arial Black"
    ARIAL_NARROW = "Arial Narrow"
    COURIER_NEW = "Courier New"
    HELVETICA = "Helvetica"
    HELVETICA_NEUE = "Helvetica Neue"
    MICROSOFT_SANS_SERIF = "Microsoft Sans Serif"
    NOTO_SANS_BATAK = "Noto Sans Batak"
    NOTO_SANS_KANNADA = "Noto Sans Kannada"
    NOTO_SANS_MYANMAR = "Noto Sans Myanmar"
    NOTO_SANS_NKO = "Noto Sans NKo"
    NOTO_SANS_ORIYA = "Noto Sans Oriya"
    NOTO_SANS_TAGALOG = "Noto Sans Tagalog"
    NOTO_SERIF_KANNADA = "Noto Serif Kannada"
    NOTO_SERIF_MYANMAR = "Noto Serif Myanmar"
    PT_MONO = "PT Mono"
    PT_SANS = "PT Sans"
    PT_SANS_CAPTION = "PT Sans Caption"
    PT_SANS_NARROW = "PT Sans Narrow"
    PT_SERIF = "PT Serif"
    PT_SERIF_CAPTION = "PT Serif Caption"
    TAHOMA = "Tahoma"
    TIMES_NEW_ROMAN = "Times New Roman"

    def __str__(self):
        return f"{self._name_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}>({self._value_})"


class FontParams(NamedTuple):
    family: FontFamily
    size: int
    weight: Literal["normal", "bold"] = NORMAL
    slant: Literal["roman", "italic"] = ROMAN
    underline: bool = False
    overstrike: bool = False

    def font(self):
        return Font(**self._asdict())
