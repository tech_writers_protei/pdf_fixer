# -*- coding: utf-8 -*-
from pathlib import Path
from tkinter import Event, Tk, Widget

from PIL import UnidentifiedImageError
from PIL.Image import open
from fitz import Page
from loguru import logger

from pdf_fixer.constants import TextBox
from pdf_fixer.gui.constants import ImageInfo, _FORMATS
from pdf_fixer.gui.gui import ActionButton, OutputText, Sketch, StepperSpinbox, WidgetType
from pdf_fixer.pdf.manager import PdfManager


class TkinterImage:
    def __init__(self, pdf_manager: PdfManager):
        self._root: Tk = Tk()
        self._widgets: dict[str, WidgetType] = dict()
        self._pdf: str | Path | None = None
        self._images: list[str | Path] = []
        self._active: int | None = None
        self._image_info: ImageInfo | None = None
        self._pdf_manager: PdfManager = pdf_manager

    def _images_str(self):
        return "\n".join(str(image) for image in self._images)

    def __str__(self):
        return f"{self._pdf}: [\n{self._images_str()}\n]\nActive: {self._active}"

    def __repr__(self):
        return (
            f"<{self.__class__.__name__}> "
            f"{self._pdf}: [\n{self._images_str()}\n]\nActive: {self._active}")

    def __len__(self):
        return len(self._images)

    def __getitem__(self, item):
        if isinstance(item, int):
            if 0 <= item < len(self):
                return self._images[item]
            else:
                logger.error(f"Ключ {item} не найден")
                raise KeyError

        else:
            logger.error(f"Ключ {item} должен быть типа int, но получен {type(item)}")
            raise TypeError

    def __bool__(self):
        return self._active is not None

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._pdf == other._pdf
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._pdf != other._pdf
        else:
            return NotImplemented

    def __add__(self, other):
        if isinstance(other, Widget):
            self._widgets[other.widgetName] = other
        else:
            logger.error(f"Виджет {other} должен быть типа Widget, но получен {type(other)}")
            return

    def get_widget(self, name: str):
        if name not in self._widgets:
            raise KeyError
        else:
            return self._widgets.get(name)

    @property
    def active(self):
        return self._active

    @active.setter
    def active(self, value):
        self._active = value

    @property
    def image_info(self):
        return self._image_info

    @image_info.setter
    def image_info(self, value):
        self._image_info = value

    @property
    def pdf(self):
        return self._pdf

    @pdf.setter
    def pdf(self, value):
        self._pdf = value

    def set_image_info(self):
        path: str | Path = self[self._active]

        try:
            with open(path, "r", _FORMATS) as i:
                image_info: ImageInfo = ImageInfo(i.width, i.height)
        except UnidentifiedImageError:
            logger.error(f"Изображение {path} не удалось обработать")
        else:
            self._image_info = image_info

    def canvas(self) -> Sketch:
        return self.get_widget("sketch")

    def text_box(self):
        return self.canvas().text_box()

    def stepper(self) -> StepperSpinbox:
        return self.get_widget("stepper_spinbox")

    def text_item(self, index: int) -> OutputText:
        return self.get_widget(f"output_text_{index}")

    def ok_button(self) -> ActionButton:
        return self._widgets.get("ok_button")

    def ok_click(self, event: Event):
        text_box: TextBox = self.canvas().text_box()
        if text_box == TextBox.null():
            return
        page: Page = self._pdf_manager.get_page(self._active)

    def set_text(self):
        top_left: str = str(self.text_box().top_left)
        bottom_right: str = str(self.text_box().bottom_right)

        self.text_item(0).set_text(top_left)
        self.text_item(1).set_text(bottom_right)

    def set_image(self):
        self._active = int(self.stepper().get())
        path: Path = self._images[self._active]
        self.canvas().set_image(path)


class AppGUI:
    def __init__(self):
        self._root: Tk = Tk()
        self._widgets: dict[str, Widget] = dict()

    def __iter__(self):
        return iter(self._widgets.items())

    def __getitem__(self, item):
        if isinstance(item, str):
            return self._widgets.get(item)
        else:
            logger.error(f"Ключ {item} не найден")
            raise KeyError

    def __add__(self, other):
        if isinstance(other, Widget):
            self._widgets[other.widgetName] = other
        else:
            logger.error(f"Элемент {other} должен быть типа Widget, но получен {type(other)}")

    def ui(self):
        for _, widget in iter(self):
            widget.grid()
        self._root.grid()
