# -*- coding: utf-8 -*-
from itertools import product
from typing import NamedTuple

from pdf_fixer.constants import Point, TextBox


class UserInput(NamedTuple):
    """Specifies the user selection of two points.

    """
    x1: int
    y1: int
    x2: int
    y2: int

    def __str__(self):
        return f"({self.x1}, {self.y1}), ({self.x2}, {self.y2})"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._values()})>"

    def _values(self):
        return ", ".join(getattr(self, attribute) for attribute in self._fields)

    @property
    def x(self):
        return sorted((self.x1, self.x2))

    @property
    def y(self):
        return sorted((self.y1, self.y2))

    def points(self) -> list[Point]:
        return [Point(x, y) for x, y in product(self.x, self.y)]

    def text_box(self) -> TextBox:
        return TextBox(self.x[0], self.y[0], self.x[1], self.y[1])
