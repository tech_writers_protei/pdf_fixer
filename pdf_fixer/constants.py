# -*- coding: utf-8 -*-
from difflib import SequenceMatcher
from pathlib import Path
from typing import Iterable, NamedTuple

from loguru import logger

ACCURACY: int = 2
CONFIDENCE: float = float(0.80)


def validate_path(path: str | Path):
    if isinstance(path, Path):
        return path

    try:
        path: Path = Path(path).resolve(True)
    except FileNotFoundError:
        logger.info(f"Путь {path} некорректен")
        raise
    else:
        return path


def multiple_compare(__values_1: Iterable[int], __values_2: Iterable[int]) -> bool:
    def single_compare(__value_1: int, __value_2: int) -> bool:
        return -ACCURACY <= __value_1 - __value_2 <= ACCURACY

    return all(single_compare(value_1, value_2) for value_1, value_2 in zip(__values_1, __values_2))


class Point(NamedTuple):
    x: int
    y: int

    def __str__(self):
        return f"x: {self.x}, y: {self.y})"

    def __repr__(self):
        return f"<{self.__class__.__name__}(x={self.x}, y={self.y})>"

    def to_list(self) -> list[int]:
        return [self.x, self.y]

    def __hash__(self):
        return hash((self.x, self.y))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.x, self.y == other.x, other.y
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.x, self.y == other.x, other.y
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, self.__class__):
            return self.x < other.x and self.y < other.y
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__):
            return self.x > other.x and self.y > other.y
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, self.__class__):
            return self.x <= other.x and self.y <= other.y
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, self.__class__):
            return self.x >= other.x and self.y >= other.y
        else:
            return NotImplemented

    def is_near(self, other) -> bool:
        if isinstance(other, self.__class__):
            return multiple_compare(self.to_list(), other.to_list())
        else:
            return NotImplemented


class TextBox(NamedTuple):
    x_min: int
    y_min: int
    x_max: int
    y_max: int

    def values(self) -> tuple[int, ...]:
        return tuple(getattr(self, attr) for attr in self._fields)

    def __str__(self):
        return f"{self.values()}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.values()})>"

    def __hash__(self):
        return hash((self.x_min, self.y_min, self.x_max, self.y_max))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.values() == other.values()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.values() != other.values()
        else:
            return NotImplemented

    def __contains__(self, item):
        if isinstance(item, self.__class__):
            _x_min: bool = self.x_min <= item.x_min
            _y_min: bool = self.y_min <= item.y_min
            _x_max: bool = self.x_max >= item.x_max
            _y_max: bool = self.y_max >= item.y_max

            return _x_min and _y_min and _x_max and _y_max
        else:
            return False

    def points(self) -> list[Point]:
        return [
            Point(self.x_min, self.y_min),
            Point(self.x_max, self.y_min),
            Point(self.x_max, self.y_max),
            Point(self.x_min, self.y_max)]

    @property
    def top_left(self) -> Point:
        return self.points()[0]

    @property
    def top_right(self) -> Point:
        return self.points()[1]

    @property
    def bottom_right(self) -> Point:
        return self.points()[2]

    @property
    def bottom_left(self) -> Point:
        return self.points()[3]

    @classmethod
    def null(cls):
        return cls(0, 0, 0, 0)

    def is_near(self, other) -> bool:
        if isinstance(other, self.__class__):
            values_1: tuple[list[int], ...] = tuple(item.to_list() for item in self.points())
            values_2: tuple[list[int], ...] = tuple(item.to_list() for item in other.points())
            return all(
                multiple_compare(value_1, value_2)
                for value_1, value_2 in zip(values_1, values_2))
        else:
            return NotImplemented


class RecognizeResult(NamedTuple):
    top_left: Point
    top_right: Point
    bottom_right: Point
    bottom_left: Point
    text: str
    confidence: float

    @property
    def points(self) -> tuple[Point, ...]:
        return self.top_left, self.top_right, self.bottom_right, self.bottom_left

    def __str__(self):
        _points_list: list[list[int]] = [point.to_list() for point in self.points]
        return f"{_points_list}, {self.text}, {self.confidence}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._asdict().values()})>"

    def __hash__(self):
        return hash([getattr(self, attribute) for attribute in self._fields])

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.points == other.points
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.points != other.points
        else:
            return NotImplemented

    def text_box(self) -> TextBox:
        return TextBox(self.top_left.x, self.top_left.y, self.bottom_right.x, self.bottom_right.y)

    def __bool__(self):
        return self.confidence >= CONFIDENCE

    def has_text(self, text: str) -> bool:
        return SequenceMatcher(None, self.text, text).quick_ratio() >= CONFIDENCE

    def has_text_box(self, text_box: TextBox) -> bool:
        return self.text_box().is_near(text_box)
