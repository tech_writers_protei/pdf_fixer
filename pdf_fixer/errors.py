# -*- coding: utf-8 -*-
class BaseError(Exception):
    """Base error class to inherit."""


class InvalidFilePathError(BaseError):
    """File path must be str or Path."""


class MissingFilePathError(BaseError):
    """File path is not specified."""


class NoneAcceptableValueError(BaseError):
    """No value has an acceptable type."""


class InvalidDocumentError(BaseError):
    """Document instance has an improper """


class MissingTextError(BaseError):
    """Text is not found."""


class InvalidPageNumberError(BaseError):
    """Page number is out of range."""
