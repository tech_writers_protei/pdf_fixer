# -*- coding: utf-8 -*-
from pathlib import Path
from typing import TypeAlias

from easyocr.easyocr import Reader
from loguru import logger

from pdf_fixer.constants import Point, RecognizeResult, TextBox, validate_path
from pdf_fixer.errors import InvalidFilePathError, MissingFilePathError

MultiList: TypeAlias = list[list[list[int]]]


class PdfRecognizer:
    languages: list[str] = ["ru", "en"]

    def __init__(self):
        self._path: str | Path | None = None
        self._results: list[RecognizeResult] = []

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        if isinstance(value, (str, Path)):
            self._path = validate_path(value)

        elif value is None:
            self._path = value

        else:
            logger.error(f"Некорректный путь {value}")
            raise InvalidFilePathError

    def __int__(self):
        return int(self._path.name.removesuffix(".png").rsplit("_", 1)[-1])

    def nullify(self):
        self._path = None
        self._results = []

    def __str__(self):
        return f"{self._path}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def __bool__(self):
        return self._path is not None

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return True
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return False
        else:
            return NotImplemented

    def set_results(self):
        if not bool(self):
            logger.error(f"Не задан путь для {self.__class__.__name__}")
            raise MissingFilePathError

        reader: Reader = Reader(
            lang_list=self.languages,
            gpu=True,
            model_storage_directory="../sources")

        text = reader.readtext(str(self._path))

        results: list[RecognizeResult] = [
            RecognizeResult(
                Point(*item[0][0]),
                Point(*item[0][1]),
                Point(*item[0][2]),
                Point(*item[0][3]),
                item[1],
                item[2]) for item in text]

        self._results = results

    def texts(self) -> list[str]:
        return [item.text for item in self._results]

    def text_boxes(self) -> list[TextBox]:
        return [item.text_box() for item in self._results]

    @property
    def results(self):
        return self._results


pdf_recognizer: PdfRecognizer = PdfRecognizer()
