# -*- coding: utf-8 -*-
from pathlib import Path
from typing import Iterable

from fitz import Document
from loguru import logger

from pdf_fixer.constants import validate_path


class PdfCombiner:
    def __init__(self):
        self._files: list[Path] = []
        self._pdf: Path | None = None

    @property
    def files(self):
        return self._files

    @files.setter
    def files(self, value):
        self._files = value

    @property
    def pdf(self):
        return self._pdf

    @pdf.setter
    def pdf(self, value):
        self._pdf = value

    def _str_files(self) -> str:
        return "\n".join(map(str, self._files))

    def __str__(self):
        return f"[{self._str_files()}]"

    def __repr__(self):
        return f"<{self.__class__.__name__}>\n{self._str_files()}\n"

    def __iter__(self):
        return iter(self._files)

    def __len__(self):
        return len(self._files)

    def __bool__(self):
        return self._pdf is not None

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return True
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return False
        else:
            return NotImplemented

    def __getitem__(self, item):
        if isinstance(item, int):
            if 0 <= item < len(self):
                return self._files[item]
            else:
                logger.error(f"Не найден ключ {item}")
                raise KeyError
        else:
            logger.error(f"Ключ {item} должен быть типа int, но получено {type(item)}")
            raise TypeError

    def __add__(self, other):
        if isinstance(other, str):
            self._files.append(validate_path(other))
            logger.info(f"Добавлен элемент {other} в {self.__class__.__name__}")

        elif isinstance(other, Path):
            self._files.append(other)
            logger.info(f"Добавлен элемент {str(other)} в {self.__class__.__name__}")

        elif isinstance(other, Iterable):
            _files_str: list[str] = list(filter(lambda x: isinstance(x, str), other))
            _files_path: list[Path] = list(filter(lambda x: isinstance(x, Path), other))
            self._files.extend(map(validate_path, _files_str))
            self._files.extend(_files_path)

            _files: str = ", ".join([str(_file) for _file in tuple(*_files_str, *_files_path)])
            logger.info(f"Добавлены элементы {_files}")

        else:
            logger.error(f"Невозможно добавить элемент {other} типа {type(other)}")

    def combine(self):
        document: Document = Document()

        for pno, file in enumerate(iter(self)):
            img: Document = Document(file)
            pdf_bytes: bytes = img.convert_to_pdf()
            img.close()
            pdf: Document = Document(filetype="pdf", stream=pdf_bytes)
            document.insert_pdf(pdf)

        document.save(self._pdf)
        logger.info(f"{self._pdf}")


pdf_combiner: PdfCombiner = PdfCombiner()
