# -*- coding: utf-8 -*-
from pathlib import Path

from PIL import Image
from fitz import Document, Page, TEXT_ALIGN_CENTER, apply_redactions
from loguru import logger

from pdf_fixer.constants import TextBox, validate_path
from pdf_fixer.errors import InvalidDocumentError, InvalidPageNumberError
from pdf_fixer.gui.constants import FontFamily
from pdf_fixer.pdf.converter import PdfConverter
from text_layer import PdfTextLayer


class PdfManager:
    def __init__(self, path: str | Path, pdf_text_layer: PdfTextLayer, converter: PdfConverter):
        self._path: Path = validate_path(path)
        self._pdf_text_layer: PdfTextLayer = pdf_text_layer
        self._document: Document = Document(self._path)
        self._converter: PdfConverter = converter

    def __str__(self):
        return f"{self._path}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._path == other._path
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._path != other._path
        else:
            return NotImplemented

    @property
    def document(self):
        return self._document

    @document.setter
    def document(self, value):
        if isinstance(value, (str, Path)):
            document: Document = validate_path(value)
            self._document = Document(document)
        elif isinstance(value, Document):
            self._document = value
        else:
            raise InvalidDocumentError

    def __getitem__(self, item):
        if isinstance(item, int):
            return self._pdf_text_layer.get(item)
        else:
            raise KeyError

    def __iter__(self):
        return iter(self._document)

    def get_image(self, page: int) -> Image:
        image_path: Path = self._converter[page]
        return Image.open(image_path)

    def get_page(self, page_no: int) -> Page:
        if 0 <= page_no < len(self._document):
            return self._document[page_no]
        else:
            logger.error(f"Страница {page_no} не найдена")
            raise InvalidPageNumberError

    def get_text_box(self, page: int, text_box: TextBox):
        for recognize_result in self[page]:
            if recognize_result.has_text_box(text_box):
                return recognize_result.text_box()
        else:
            logger.error(f"TextBox {str(text_box)} не найден")
            return None

    def replace(
            self,
            page_no: int,
            text_box: TextBox,
            text: str,
            font_family: FontFamily = FontFamily.TIMES_NEW_ROMAN,
            font_size: int = 14):
        page: Page = self.get_page(page_no)
        page.add_redact_annot(
            text_box.values(),
            text,
            font_family.value,
            font_size,
            TEXT_ALIGN_CENTER,
            cross_out=False)
        apply_redactions(page, images=0, graphics=0)
