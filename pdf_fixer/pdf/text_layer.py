# -*- coding: utf-8 -*-
from pathlib import Path
from typing import Iterable

from loguru import logger

from pdf_fixer.constants import RecognizeResult, TextBox, validate_path
from pdf_fixer.errors import InvalidFilePathError, NoneAcceptableValueError


class PdfTextLayer:
    def __init__(self):
        self._text: dict[int, list[RecognizeResult]] = dict()
        self._path: Path | None = None

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        if isinstance(value, (str, Path)):
            _: Path = validate_path(value)
            self._path = str(_)

        elif value is None:
            self._path = value

        else:
            raise InvalidFilePathError

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, value):
        self._text = value

    def _text_str(self) -> list[str]:
        _text: list[str] = []

        for k, v in self._text.items():
            _text_item: str = ", ".join(map(lambda x: x.text, v))
            _text.append(f"page {k}:\n{_text_item}")

        return _text

    def __str__(self):
        return f"{self._path}: {self._text_str()}"

    def __repr__(self):
        return f"<{self.__class__.__name__}>: {self._path}\n{self._text_str()}"

    def __len__(self):
        return len(self._text)

    def __bool__(self):
        return len(self._text) > 0

    def __getitem__(self, item):
        if isinstance(item, int):
            if 0 <= item < len(self):
                return self._text.get(item)
            else:
                logger.error(f"Ключ {item} не найден")
                raise KeyError
        else:
            logger.error(f"Ключ {item} должен быть типа int, но получен {type(item)}")
            raise TypeError

    get = __getitem__

    def __setitem__(self, key, value):
        if isinstance(key, int):
            if isinstance(value, RecognizeResult):
                self._text[key].append(value)

            elif isinstance(value, Iterable):
                values: list[RecognizeResult] = [
                    filter(lambda x: isinstance(x, RecognizeResult), value)]

                if not values:
                    logger.error(f"Не найдено ни одно значение типа RecognizeResult")
                    raise NoneAcceptableValueError
                else:
                    self._text[key].extend(values)

        else:
            logger.error(
                f"Ключ {key} должен быть типа int, а значение {value} - RecognizeResult,"
                f" но получены {type(key)} и {type(value)}")
            raise TypeError

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return True
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return False
        else:
            return NotImplemented

    def keys(self) -> set[int]:
        return {*self._text.keys()}

    def values(self) -> set[list[RecognizeResult]]:
        return {*self._text.values()}

    def items(self):
        return self._text.items()

    @property
    def texts(self) -> list[str]:
        return [item.text for value in self._text.values() for item in value]

    def text_boxes(self, page: int) -> list[TextBox]:
        return [item.text_box() for item in self.get(page)]

    def get_text_box(
            self,
            page: int, *,
            text_box: TextBox | None = None,
            text: str | None = None) -> TextBox | None:
        if text_box is not None:
            return self._find_text_box(page, text_box)
        elif text is not None:
            return self._find_text(page, text)
        else:
            raise ValueError

    def _find_text_box(self, page: int, text_box: TextBox):
        for item in self.get(page):
            if item.has_text_box(text_box):
                return item
        else:
            logger.error(f"TextBox {str(text_box)} не найден")
            return

    def _find_text(self, page: int, text: str):
        for item in self.get(page):
            if item.has_text(text):
                return item
        else:
            logger.error(f"Текст {text} не найден")
            return


pdf_text_layer: PdfTextLayer = PdfTextLayer()
