# -*- coding: utf-8 -*-
from pathlib import Path

from fitz import Document, Page, Pixmap
from fitz.utils import get_pixmap
from loguru import logger

from pdf_fixer.constants import validate_path
from pdf_fixer.errors import InvalidFilePathError, MissingFilePathError


class PdfConverter:
    def __init__(self):
        self._path: Path | None = None
        self._generated_images: list[Path] = []

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        if isinstance(value, (str, Path)):
            self._path = validate_path(value)

        elif value is None:
            self._path = value

        else:
            logger.error(f"Значение {value} должно быть типа str, Path или None, но получено {type(value)}")
            raise InvalidFilePathError

    def __str__(self):
        return f"{self._path}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return True
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return False
        else:
            return NotImplemented

    def __len__(self):
        return len(self._generated_images)

    def __bool__(self):
        return self._path is not None

    def __iter__(self):
        return iter(self._generated_images)

    def __getitem__(self, item):
        if isinstance(item, int):
            if 0 <= item < len(self):
                return self._generated_images[item]
            else:
                logger.error(f"Ключ {item} не найден")
                raise KeyError
        else:
            logger.error(f"Ключ {item} должен быть типа int, но получен {type(item)}")
            raise TypeError

    def convert(self):
        if not bool(self):
            raise MissingFilePathError

        document: Document = Document(self._path)

        page: Page
        for index, page in enumerate(iter(document)):
            image: Path = self._path.with_name(f"{self._path.stem}_page_{index}.png")

            logger.info(f"image = {image}")
            pixmap: Pixmap = get_pixmap(page, dpi=300)
            pixmap.save(image, "png")

            self._generated_images.append(image)

        return

    @property
    def generated_images(self):
        return self._generated_images


pdf_converter: PdfConverter = PdfConverter()
