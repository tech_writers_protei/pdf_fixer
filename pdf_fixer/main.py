# -*- coding: utf-8 -*-
from pathlib import Path

from loguru import logger

from pdf_fixer.pdf.combiner import pdf_combiner
from pdf_fixer.pdf.converter import pdf_converter
from pdf_fixer.pdf.recognizer import pdf_recognizer
from pdf_fixer.pdf.text_layer import pdf_text_layer


def main():
    path: Path = Path("/Users/andrewtarasov/Desktop/relocate/test_pdf.pdf").resolve()
    pdf_converter.path = path
    pdf_converter.convert()

    pdf_combiner.pdf = path.with_stem(f"{path.stem}_ocr")
    pdf_combiner.files = pdf_converter.generated_images

    logger.info(f"{pdf_combiner.files}")

    for generated_image in iter(pdf_converter.generated_images):
        pdf_recognizer.nullify()
        pdf_recognizer.path = generated_image

        logger.info(f"{pdf_recognizer.path}")
        pdf_recognizer.set_results()

        index: int = int(pdf_recognizer)
        logger.info(f"index = {index}")

        if index not in pdf_text_layer.text:
            pdf_text_layer.text[index] = []

        pdf_text_layer[int(pdf_recognizer)] = pdf_recognizer.results

    pdf_combiner.combine()

    logger.info(f"{pdf_text_layer.texts}")


if __name__ == '__main__':
    from tkinter import font, Tk

    root: Tk = Tk()

    for family in font.families():
        print(family)
